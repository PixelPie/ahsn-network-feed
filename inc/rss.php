<?php
define('AHSN_NETWORK_FEED_ALLOWED_TAGS', '<img><blockquote><cite><a><b><strong><i><li><del><strike><ol><ul><u><sup><pre><code><sub><hr><h1><h2><h3><h4><h5><h6><big><small><p><br><span><video><audio><dd><dl>');
add_action( 'parse_request', 'ahsn_network_feed_parse_request' );
add_filter( 'query_vars', 'ahsn_network_feed_query_vars' );
function ahsn_network_feed_query_vars( $query_vars ){
  $query_vars[] = 'ahsn_network_feed';
  return $query_vars;
}
function ahsn_network_feed_parse_request( $wp ) {
  if ( array_key_exists( 'ahsn_network_feed', $wp->query_vars ) ) {
    $call_ahsn_network_feed = $wp->query_vars['ahsn_network_feed'];
    if($call_ahsn_network_feed=='1') call_ahsn_network_rss();
    die();
  }
}
register_activation_hook(__FILE__, 'ahsn_network_feed_activation');
function ahsn_network_feed_activation() {
	ahsn_network_feed_set_defaults();
}

register_deactivation_hook(__FILE__, 'ahsn_network_feed_deactivation');
function ahsn_network_feed_deactivation() {
	delete_option( 'custom_simple_rss_options' );
}

function ahsn_network_feed_set_defaults(){
  $custom_simple_rss_options	= array(
    'csrp_post_type'=> 'post',
    'csrp_post_status'=> 'publish',
    'csrp_posts_per_page'=> 2,
    'csrp_show_meta'=> 0,
    'csrp_show_thumbnail'=> 1,
    'csrp_show_content'=> 1,
    'csrp_allowed_tags' => AHSN_NETWORK_FEED_ALLOWED_TAGS,
    'csrp_secret_key'=> '',
    'csrp_xml_type'=> 0, //0 = string, 1 = DOM
    'csrp_pubdate_date_format'=> 'rfc',
  );
  update_option('custom_simple_rss_options',$custom_simple_rss_options);
  return $custom_simple_rss_options;
}
function call_ahsn_network_rss() {
  $custom_simple_rss_options = false;
  //get_option('custom_simple_rss_options');
  if(is_array($custom_simple_rss_options)===false){
    //set defaults and return array
    $custom_simple_rss_options = ahsn_network_feed_set_defaults();
  }
  $csrp_show_post_terms = $csrp_debug = $csrp_show_all_post_terms = null;
  extract($custom_simple_rss_options);
  $options['csrp_show_thumbnail'] = $csrp_show_thumbnail;
  $options['csrp_show_post_terms'] = $csrp_show_post_terms;
  $options['csrp_allowed_tags'] = $csrp_allowed_tags;
  $options['csrp_pubdate_date_format'] = $csrp_pubdate_date_format;
  /*
  =======================
  GET
  =========================
  */
  if( isset($_GET["csrp_debug"]) ) $csrp_debug = intval($_GET["csrp_debug"]);
    $options['csrp_debug'] = $csrp_debug;

  if($csrp_allowed_tags=='' || empty($csrp_allowed_tags)){
    $csrp_allowed_tags = AHSN_NETWORK_FEED_ALLOWED_TAGS;
  }
    $options['csrp_allowed_tags'] = $csrp_allowed_tags;

  if( isset($_GET["type"]) ){
    $csrp_post_type = sanitize_text_field($_GET["type"]);
    $got_type = sanitize_text_field($_GET["type"]);
  } else {
    $csrp_post_type = 'news';
    $got_type = 'news';
  }

  /*
  =======================
  Convert url options to actual selected values in plugin
  =========================
  */
  if ($csrp_post_type == 'news') {
    // Get options from settings
    $csrp_post_type = get_option('ahsn_network_feed_field_news');
    $chosen_categories = get_option('ahsn_network_feed_field_news_categories');
  } elseif ($csrp_post_type == 'events') {
    // Get options from settings
    $csrp_post_type = get_option('ahsn_network_feed_field_events');
    $chosen_categories = get_option('ahsn_network_feed_field_events_categories');
  } elseif ($csrp_post_type == 'innovations') {
    // Get options from settings
    $csrp_post_type = get_option('ahsn_network_feed_field_innovations');
    $chosen_categories = get_option('ahsn_network_feed_field_innovations_categories');
  } elseif ($csrp_post_type == 'challenges') {
    // Get options from settings
    $csrp_post_type = get_option('ahsn_network_feed_field_challenges');
    $chosen_categories = get_option('ahsn_network_feed_field_challenges_categories');
  }
  /*
  =======================
  Bring in categories options to proper array
  =========================
  */
  $categories = [];
  if (!empty($chosen_categories)) {
    foreach ($chosen_categories as $key => $cat) {
      $cat_details = explode('--', $cat);
      $categories[$cat_details[0]][$cat_details[1]] = $cat_details[1];
    }
  }

  /*
  =======================
  args
  =========================
  */
  $args = array(
    'post_type' => $csrp_post_type,
    'showposts' => 2,
    'post_status'=> 'publish',
    'ignore_sticky_posts' => true,
  );

  if (!empty($categories) && count($categories)) {
    $tax_term_id_array = [];
    $args['tax_query'] = Array(
      'relation' => 'OR',
    );
    foreach ($categories as $tax => $terms) {
      foreach ($terms as $term_id) {
        $tax_term_id_array[$term_id] = $term_id;
      }
      $args['tax_query'][] = Array(
        'taxonomy' => $tax,
        'field' => 'term_id',
        'terms' => $tax_term_id_array,
        'operator' => 'IN',
      );
    }
  }

  $namespaces = array(
    "content" => "http://purl.org/rss/1.0/modules/content/",
    "wfw" => "http://wellformedweb.org/CommentAPI/",
    "dc" => "http://purl.org/dc/elements/1.1/",
    "atom" => "http://www.w3.org/2005/Atom",
    "sy" => "http://purl.org/rss/1.0/modules/syndication/",
    "slash" => "http://purl.org/rss/1.0/modules/slash/",
    "media" => "http://search.yahoo.com/mrss/",
    "wp" => "http://wordpress.org/export/1.2/",
    "excerpt" => "http://wordpress.org/export/1.2/excerpt/",
  );
  $options['namespaces'] = $namespaces;

  $csrp_feed_output = null;
  $csrp_feed_output = csrp_build_xml_string($args,$options);

  header('Content-Type: text/xml; charset=utf-8');
  print($csrp_feed_output);

}

function csrp_build_xml_string($args,$options){

  extract($options);

  if( isset($_GET["type"]) ){
    $got_type = sanitize_text_field($_GET["type"]);
  } else {
    $got_type = 'news';
  }
  /*=================== the query ===================
  *=================================================*/
  $the_query = new WP_Query( $args );

  $namespaces_str = '';
  foreach($namespaces as $name => $value){
    $namespaces_str .=  'xmlns:'.$name.'="'.$value.'" ';
  }
  $csrp_feed_current = '<?xml version="1.0" encoding="UTF-8"?>
  <rss version="2.0" '.$namespaces_str.' >';

  $csrp_feed_current .= '
  <channel>
    <title>'.get_bloginfo("name").'</title>
    <description>'.get_bloginfo("description").'</description>
    <link>'.get_home_url().'</link>
    <lastBuildDate>'.  mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false) .'</lastBuildDate>';

  $debug_data = array(
    'args' => $args,
    'options' => $options,
  );

  if(isset($csrp_debug)&& $csrp_debug=='1') $csrp_feed_current .=	'<debug>'.json_encode($debug_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE).'</debug>';

  if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
      $the_query->the_post();
      $post_id = get_the_ID();
      $the_post = get_post($post_id);
      $excerpt = $the_post->post_excerpt;
      $modified = $the_post->post_modified;
      $created = $the_post->post_date;
      $author_id = $the_post->post_author;
      $author = get_the_author_meta('display_name', $author_id );
      switch ($csrp_pubdate_date_format) {
        case "rfc":
          $date_format =  'D, d M Y H:i:s O';
          $pub_date = get_the_date( $date_format, $post_id );
          break;
        case "blog_date":
          $date_format =  get_option( 'date_format' );
          $pub_date = get_the_date( $date_format, $post_id );
          break;
        case "blog_date_time":
          $date_format =  get_option( 'date_format' ).' '.get_option('time_format');
          $pub_date = get_the_date( $date_format, $post_id );
          break;
        default:
          $date_format =  'D, d M Y H:i:s O';
          $pub_date = get_the_date( $date_format, $post_id );
      }

      $the_content = apply_filters('the_content',get_the_content($post_id));
      $allowed_tags = $csrp_allowed_tags;
      $the_content = htmlspecialchars_decode($the_content,ENT_NOQUOTES);
      $the_content = strip_tags($the_content,$allowed_tags);
      $the_content = preg_replace("/\r?\n/", "", $the_content);
      $the_content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $the_content);
      $the_content = preg_replace('/\s+/',' ',$the_content); //tabs
      $the_content = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $the_content);

      $csrp_feed_current .='
      <item>
        <title>'. get_the_title($post_id) .'</title>
        <link>'. get_permalink($post_id) .'</link>
        <pubDate>'. $pub_date .'</pubDate>
        <dc:creator>'. $author .'</dc:creator>
        <dc:identifier>'.  $post_id .'</dc:identifier>
        <dc:modified>'. $modified .'</dc:modified>
        <dc:created unix="'. strtotime($created).'">'. $created .'</dc:created>
        <guid isPermaLink="true">'. get_permalink($post_id) .'</guid>';
        if ($got_type == 'news' || $got_type == 'events') {
          $csrp_feed_current .= '<description><![CDATA['. $excerpt .']]></description>';
        }
        if($got_type != 'innovations') {
          if (!empty($the_content)) {
            $csrp_feed_current .='<content><![CDATA['. $the_content .']]></content>';
          } else {
            $csrp_feed_current .= '<content><![CDATA[<a href="' . get_permalink($post_id) . '" target="_blank" rel="noopener" class="btn btn--external-rss" style="margin:0 auto;display: block;max-width: 460px;">Read this on the '.get_bloginfo("name").' website.</a>]]></content>';
          }
        }
        if (has_post_thumbnail(get_the_id())) {
          $featimg = get_the_post_thumbnail_url(get_the_id(), 'full');
          if(!empty($featimg)) {
            $csrp_feed_current .= '<featured_image>'. $featimg .'</featured_image>';
          }
        }
        // Custom event fields
        if ($got_type == 'events' && !empty(get_option('ahsn_network_feed_field_events_date'))) {
          $date = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_events_date'));
          if(!empty($date[0])) {
            if (!is_numeric($date[0])) {
              $csrp_feed_current .= '<event_date>'. strtotime($date[0]) .'</event_date>';
            } else {
              $csrp_feed_current .= '<event_date>'. $date[0] .'</event_date>';
            }
          }
        }
        if ($got_type == 'events' && !empty(get_option('ahsn_network_feed_field_events_register'))) {
          $register = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_events_register'));
          if(!empty($register[0])) {
            $csrp_feed_current .= '<register_link>'. $register[0] .'</register_link>';
          }
        }
        // Custom Innovations fields
        if (sanitize_text_field($got_type) == 'innovations') {
          if (!empty(get_option('ahsn_network_feed_field_innovations_intro'))) {
            $introduction = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_innovations_intro'));
            if(!empty($introduction[0])) {
              $csrp_feed_current .= '<description><![CDATA['. $introduction[0] .']]></description>';
            }
          }
          if (!empty(get_option('ahsn_network_feed_field_innovations_content'))) {
            $body = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_innovations_content'));
            if(!empty($body[0])) {
              $csrp_feed_current .= '<content><![CDATA['. $body[0] .']]></content>';
            } else {
              $csrp_feed_current .= '<content><![CDATA[<a href="' . get_permalink($post_id) . '" target="_blank" rel="noopener" class="btn btn--external-rss" style="margin:0 auto;display: block;max-width: 460px;">View this innovation on the '.get_bloginfo("name").' website.</a>]]></content>';
            }
          } else {
            if (empty(get_option('ahsn_network_feed_field_innovations_intro'))) {
              // Regex content to split into relevant sections
              // Find headings
              $separated_content = ahsnnf_find_intro($the_content);
              if ($separated_content != false) {
                $the_content = $separated_content[1];
                $csrp_feed_current .= '<description><![CDATA['. $separated_content[0] .']]></description>';
              }
            }
            $csrp_feed_current .= '<content><![CDATA[<a href="' . get_permalink($post_id) . '" target="_blank" rel="noopener" class="btn btn--external-rss" style="margin:0 auto;display: block;max-width: 460px;">View this innovation on the '.get_bloginfo("name").' website.</a>]]></content>';
          }
          if (!empty(get_option('ahsn_network_feed_field_innovations_organisation'))) {
            $organisation = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_innovations_organisation'));
            if(!empty($organisation[0])) {
              $csrp_feed_current .= '<organisation>'. $organisation[0] .'</organisation>';
            }
          }

          // Fetch images data
          $images_array = ahsn_network_feed_search_data('ahsn_network_feed_field_innovations_images');
          if ($images_array) {
            // Loop through array and fetch content
            foreach ($images_array as $img_key => $img_val) {
              $img_url = ahsn_network_feed_fetch_img_url($img_val);
              if ($img_url) {
                $csrp_feed_current .= '<imagefile>'. $img_url .'</imagefile>';
              }
            }
          }

          // Fetch files data
          $files_array = ahsn_network_feed_search_data('ahsn_network_feed_field_innovations_files');
          if ($files_array) {
            // Loop through array and fetch content
            foreach ($files_array as $file_key => $file_val) {
              $file_url = ahsn_network_feed_fetch_img_url($file_val);
              if ($file_url) {
                $csrp_feed_current .= '<file>'. $file_url .'</file>';
              }
            }
          }

          // Fetch videos data
          $videos_array = ahsn_network_feed_search_data('ahsn_network_feed_field_innovations_videos');
          if ($videos_array) {
            // Loop through array and fetch content
            foreach ($videos_array as $vid_key => $vid_val) {
              $vid_url = ahsn_network_feed_parse_videos($vid_val);
              if (!empty($vid_url) && !is_array($vid_url)) {
                $csrp_feed_current .= '<video>'. $vid_url .'</video>';
              } else {
                foreach ($vid_url as $vid_url_key => $deeper_url) {
                  $csrp_feed_current .= '<video>'. $deeper_url .'</video>';
                }
              }
            }
          }

        }
      $csrp_feed_current .='
      </item>';
    }
  } else {
    // no posts found
  }

  /* Restore original Post Data */
  wp_reset_postdata();

  $csrp_feed_current .='</channel></rss><!-- end of xml string -->';

  return $csrp_feed_current;
}