<?php
/**
 * SETTINGS CHECK
 * --
 * A quick check to see if plugin has had any data saved to it
 */
function ahsn_network_feed_settings_check () {
  $settings = false;
  if (get_option('ahsn_network_feed_field_news') ||
   get_option('ahsn_network_feed_field_events') ||
   get_option('ahsn_network_feed_field_innovations') ||
   get_option('ahsn_network_feed_field_challenges')) {
    $settings = true;
  }
  return $settings;
}

/**
 * SHOWS POSTS AND DATA SHARED
 * --
 * A helpful visual to show whats currently being shared on the RSS feed
 */
function ahsn_network_feed_show_dash_posts($args, $chosen_categories, $feed_type) {
  $categories = [];
  if (!empty($chosen_categories)) {
    foreach ($chosen_categories as $key => $cat) {
      $cat_details = explode('--', $cat);
      $categories[$cat_details[0]][$cat_details[1]] = $cat_details[1];
    }
  }
  if (!empty($categories) && count($categories)) {
    $tax_term_id_array = [];
    $args['tax_query'] = Array(
      'relation' => 'AND',
    );
    foreach ($categories as $tax => $terms) {
      foreach ($terms as $term_id) {
        $tax_term_id_array[$term_id] = $term_id;
      }
      $args['tax_query'][] = Array(
        'taxonomy' => $tax,
        'field' => 'term_id',
        'terms' => $tax_term_id_array,
        'operator' => 'IN',
      );
    }
  }
  $dashboard_query = new WP_Query( $args );
  if ( $dashboard_query->have_posts() ) : ?>
    <div id="dashboard-widgets-wrap">
      <div id="dashboard-widgets" class="metabox-holder">
        <?php $i = 1; ?>
        <?php while ( $dashboard_query->have_posts() ) : $dashboard_query->the_post(); ?>
          <div id="postbox-container-<?php echo $i; ?>" class="postbox-container">
            <div class="card meta-box-sortables" style="margin-bottom:30px;">
              <?php
              if (has_post_thumbnail()) {
                the_post_thumbnail( 'medium' );
              }
              ?>
              <h2 class="title"><?php the_title(); ?></h2>
              <?php
              if ($feed_type == 'news' || $feed_type == 'challenges') : ?>
                <?php the_excerpt(); ?>
              <?php elseif ($feed_type == 'events') : ?>
                <?php the_excerpt();
                if (!empty(get_option('ahsn_network_feed_field_events_date'))) {
                  $date = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_events_date'));
                  if(!empty($date[0])) {
                    if (!is_numeric($date[0])) {
                      ahsnnf_display_meta_field('Event date:', date('jS F Y H:i', strtotime($date[0])));
                    } else {
                      ahsnnf_display_meta_field('Event date:', date('jS F Y H:i', $date[0]));
                    }
                  }
                }
                if (!empty(get_option('ahsn_network_feed_field_events_register'))) {
                  $register = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_events_register'));
                  if(!empty($register[0])) {
                    ahsnnf_display_meta_field('Registration link:', $register[0]);
                  }
                }
                ?>
              <?php elseif ($feed_type == 'innovations') : ?>
                <?php
                  if (!empty(get_option('ahsn_network_feed_field_innovations_organisation'))) {
                    $organisation = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_innovations_organisation'));
                    if(!empty($organisation[0])) {
                      ahsnnf_display_meta_field('Organisation:', $organisation[0]);
                    }
                  }
                  if (!empty(get_option('ahsn_network_feed_field_innovations_intro'))) {
                    $introduction = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_innovations_intro'));
                    if(!empty($introduction[0])) {
                      ahsnnf_display_meta_field('Introduction:', '<br>'.$introduction[0]);
                    }
                  }
                  if (!empty(get_option('ahsn_network_feed_field_innovations_content'))) {
                    $body = get_post_meta(get_the_id(), get_option('ahsn_network_feed_field_innovations_content'));
                    if(!empty($body[0])) {
                      ahsnnf_display_meta_field('Body:', '<br>'.$body[0]);
                    }
                  } else {
                    $the_content = apply_filters('the_content',get_the_content());
                    $allowed_tags = '<img><blockquote><cite><a><b><strong><i><li><del><strike><ol><ul><u><sup><pre><code><sub><hr><h1><h2><h3><h4><h5><h6><big><small><p><br><span><video><audio><dd><dl>';
                    $the_content = htmlspecialchars_decode($the_content,ENT_NOQUOTES);
                    $the_content = strip_tags($the_content,$allowed_tags);
                    $the_content = preg_replace("/\r?\n/", "", $the_content);
                    $the_content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $the_content);
                    $the_content = preg_replace('/\s+/',' ',$the_content); //tabs
                    $the_content = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $the_content);
                    if (empty(get_option('ahsn_network_feed_field_innovations_intro'))) {
                      // Regex content to split into relevant sections
                      // Find headings
                      $separated_content = ahsnnf_find_intro($the_content);
                      if ($separated_content != false) {
                        $the_content = $separated_content[1];
                        ahsnnf_display_meta_field('Introduction:', '<br>'.$separated_content[0]);
                      }
                    }
                    ahsnnf_display_meta_field('Body:', '<br>'.wp_trim_words(strip_tags($the_content)));
                  }

                  // Fetch images data
                  $images_array = ahsn_network_feed_search_data('ahsn_network_feed_field_innovations_images');
                  if ($images_array) {
                    // Loop through array and fetch content
                    foreach ($images_array as $img_key => $img_val) {
                      $img_url = ahsn_network_feed_fetch_img_url($img_val);
                      if ($img_url) {
                        ahsnnf_display_meta_field('Image:', '<br><img src="'.$img_url.'">');
                      }
                    }
                  }

                  // Fetch files data
                  $files_array = ahsn_network_feed_search_data('ahsn_network_feed_field_innovations_files');
                  if ($files_array) {
                    // Loop through array and fetch content
                    foreach ($files_array as $file_key => $file_val) {
                      $file_url = ahsn_network_feed_fetch_img_url($file_val);
                      if ($file_url) {
                        ahsnnf_display_meta_field('File:', '<a href="'.$file_url.'">'.$file_url.'</a>');
                      }
                    }
                  }

                  // Fetch videos data
                  $videos_array = ahsn_network_feed_search_data('ahsn_network_feed_field_innovations_videos');
                  if ($videos_array) {
                    // Loop through array and fetch content
                    foreach ($videos_array as $vid_key => $vid_val) {
                      $vid_url = ahsn_network_feed_parse_videos($vid_val);
                      if (!empty($vid_url) && !is_array($vid_url)) {
                        ahsnnf_display_meta_field('Video:', '<a href="'.$vid_url.'">'.$vid_url.'</a>');
                      } else {
                        foreach ($vid_url as $vid_url_key => $deeper_url) {
                          ahsnnf_display_meta_field('Video:', '<a href="'.$deeper_url.'">'.$deeper_url.'</a>');
                        }
                      }
                    }
                  }
                  ?>
              <?php endif; ?>
              <a href="<?php the_permalink(); ?>" class="button">View</a>
            </div>
          </div>
          <?php $i++; ?>
        <?php endwhile; ?>
      </div>
    </div>
  <?php else : ?>
    <p><strong>No content found yet.</strong> If you think content should be showing here, please <a href="/wp-admin/admin.php?page=ahsn-network-feed-settings">double check your settings</a>.</p>
  <?php endif;
  echo '<hr style="clear:both;">';
  wp_reset_postdata();
}

/**
 * GET ALL META KEYS FROM DB FOR CONTENT TYPE
 * --
 * This helps the function below by initially getting all
 * meta keys from a supplied content type from the WP database
 */
function ahsn_network_feed_generate_custom_meta_keys($type){
  global $wpdb;
  $post_type = $type;
  $query = "
      SELECT DISTINCT($wpdb->postmeta.meta_key)
      FROM $wpdb->posts
      LEFT JOIN $wpdb->postmeta
      ON $wpdb->posts.ID = $wpdb->postmeta.post_id
      WHERE $wpdb->posts.post_type = '%s'
      AND $wpdb->postmeta.meta_key != ''
      AND $wpdb->postmeta.meta_key NOT RegExp '(^[0-9].+$)'
      AND $wpdb->postmeta.meta_key NOT RegExp '(^[0-9]+$)'
  ";
  $meta_keys = $wpdb->get_col($wpdb->prepare($query, $post_type));
  return $meta_keys;
}

/**
 * GET ALL META KEYS / FIELDS FROM CONTENT TYPE
 * --
 * This checks all field data from a supplied content type name
 * It then removes any duplications and standard WP data, leaving only data fields
 */
function ahsn_network_feed_get_meta_keys($type){
  // Get all meta keys
  $meta_keys = ahsn_network_feed_generate_custom_meta_keys($type);
  // Set vars
  $previous_meta_val = '';
  $arrayed_duplicates = array();
  // Firstly loop through all keys
  foreach ($meta_keys as $meta_key => $meta_val) {
    // Remove any keys that are duplicated with underscore ahead of them as these are not used for content
    $first_char = substr($meta_val, 0, 1);
    if ($first_char == '_') {
      $mod_meta_val = substr($meta_val, 1);
      if ($mod_meta_val == $previous_meta_val) {
        unset($meta_keys[$meta_key]);
      }
    }
    // Remove any plugin metafields
    if (substr($meta_val, 0, 12) == '_yoast_wpseo' ||
    substr($meta_val, 0, 10) == '_searchwp_' ||
    substr($meta_val, 0, 8) == '_wp_old_' ||
    substr($meta_val, 0, 13) == '_cornerstone_') {
      unset($meta_keys[$meta_key]);
    }
    // Check if part of an arrayed value set
    if (strpos($meta_val, '_0_') !== false) {
      $temp_val = preg_replace("/_[0-9]_/", "_X_", $meta_val);
      $temp_val = preg_replace("/_[0-9][0-9]_/", "_X_", $temp_val);
      $temp_val = preg_replace("/_[0-9][0-9][0-9]_/", "_X_", $temp_val);
      $arrayed_duplicates[] = $temp_val;
    } else {
      $temp_meta = preg_replace("/_[0-9]_/", "_X_", $meta_val);
      $temp_meta = preg_replace("/_[0-9][0-9]_/", "_X_", $temp_meta);
      $temp_meta = preg_replace("/_[0-9][0-9][0-9]_/", "_X_", $temp_meta);
      if (in_array($temp_meta, $arrayed_duplicates)) {
        unset($meta_keys[$meta_key]);
      }
    }
    // Strip standard WP status ones
    if($meta_val == '_edit_lock' ||
    $meta_val == '_type' ||
    $meta_val == 'type' ||
    $meta_val == '_edit_last' ||
    $meta_val == '_pingme' ||
    $meta_val == '_encloseme' ||
    $meta_val == '_wp_page_template') {
      unset($meta_keys[$meta_key]);
    }
    // Set previous variable
    $previous_meta_val = $meta_val;
  }
  return $meta_keys;
}

/**
 * FETCH DATA FROM ANY ARRAYS
 * --
 * This checks the data to see if it's an array,
 * or an array within an array... then returns a simple array of values
 */
function ahsn_network_feed_fetch_arrayed_data($any_data) {
  // Firstly, abandon if this isn't an array
  if (empty($any_data) || !is_array($any_data)) {
    return $any_data;
  }
  if (is_array($any_data[0])) {
    // If we are dealing with a multi level array, flatten it all
    $flattened_array = call_user_func_array('array_merge', $any_data);
  } else {
    $flattened_array = $any_data;
  }
  if (!empty($flattened_array)) return $flattened_array;
  return false;
}


/**
 * FETCH IMAGE / FILE FROM ANY DATA
 * --
 * This checks the data to see if it's an integer for an
 * attachment url and makes sure what is returned is a url
 */
function ahsn_network_feed_fetch_img_url($data) {
  // Check if data is an integer
  if ($num_val = ahsn_network_feed_is_integer($data)) {
    // If so, attempt loading of attachment url
    $image_url = wp_get_attachment_url($num_val);

  // If data is not integer
  } else {
    // Do nothing for now, just pass variable
    $image_url = $data;
  }
  // check url is now an actual url, if not, leave it
  $image_url = (substr($image_url, 0, 4) == 'http') ? $image_url : FALSE;
  // Return false or image url
  return $image_url;
}

/**
 * FETCH VIDEO URL FROM ANY DATA
 * --
 * This checks the data to see if it is a genuine
 * YouTube or Vimeo video and returns the url for it
 */
function ahsn_network_feed_fetch_video_url($data) {
  // Check if data is an integer
  if ($num_val = ahsn_network_feed_is_integer($data)) {
    // If so, attempt loading of attachment url
    $image_url = wp_get_attachment_url($num_val);

  // If data is not integer
  } else {
    // Do nothing for now, just pass variable
    $image_url = $data;
  }
  // check url is now an actual url, if not, leave it
  $image_url = (substr($image_url, 0, 4) == 'http') ? $image_url : FALSE;
  // Return false or image url
  return $image_url;
}

/**
 * Checks if number is integer
 */
function ahsn_network_feed_is_integer($input){
  // Checks if number is parseable and an integer
  if (ctype_digit(strval($input))) {
    // Return actual value as number
    return intval($input);
  } else {
    return false;
  }
}

/**
 * Creates readable meta key name from supplied key
 */
function ahsn_network_feed_readable_metakey($key) {
  $key = str_replace('_0_', ' > ', $key);
  $key = str_replace('_', ' ', $key);
  return ucwords($key);
}

/**
 * GET MULTIPLE PIECES OF DATA FROM FIELDS
 * --
 * This searches the posts meta fields for the correct pieces of data
 */
function ahsn_network_feed_search_data($option_var) {
  // Check option exists
  if (!empty($option_var) && !empty($option_val = get_option($option_var))) {

    // Check if meta key is actually part of other meta keys
    if (strpos($option_val, '_0_') !== false) {
      // If so, (like ACF repeater), find these other meta key values
      $val_check = $option_val;
      $previous_num = 0;
      $content_array = array();

      // Check for content through increasing number
      while (!empty(get_post_meta(get_the_id(), $val_check))) {
        // Add to array if exists
        $content_array[] = get_post_meta(get_the_id(), $val_check);
        // Write new content location
        $new_num = $previous_num + 1;
        $val_check = str_replace('_'.$previous_num.'_', '_'.$new_num.'_', $val_check);
        $previous_num = $new_num;
        // If empty while stops
      }

      // If legitimate array has been created
      if (!empty($content_array) && is_array($content_array)) {
        // Flatten array
        $content_array = ahsn_network_feed_fetch_arrayed_data($content_array);
      }

    // Else meta key is either a single value or an array of values
    } else {

      // Get initial data from post meta using current option
      $data = get_post_meta(get_the_id(), $option_val);
      // If is legitimate array
      if (!empty($data) && $data != '' && is_array($data)) {
        // Flatten array
        $content_array = ahsn_network_feed_fetch_arrayed_data($data);

      // If is single string or number
      } elseif (!empty($data) && $data != '') {
        // Convert to single item array
        $content_array[] = $data;
      }
    }

    // Return array of content
    if (!empty($content_array) && is_array($content_array)) {
      return $content_array;
    }
  }
  return false;
}

/**
 * PARSE STRING FOR YOUTUBE / VIMEO VIDEOS
 *
 * @param       string  $video_string
 * @return      array   A video url if found
 *
 */
function ahsn_network_feed_parse_videos($video_string = null) {
    // return data
    $videos = array();

    if (!empty($video_string)) {

        // split on line breaks
        $video_string = stripslashes(trim($video_string));
        $video_string = explode("\n", $video_string);
        $video_string = array_filter($video_string, 'trim');

        // check each video for proper formatting
        foreach ($video_string as $video) {

            // check for iframe to get the video url
            if (strpos($video, 'iframe') !== FALSE) {
                // retrieve the video url
                $anchorRegex = '/src="(.*)?"/isU';
                $results = array();
                if (preg_match($anchorRegex, $video, $results)) {
                    $link = trim($results[1]);
                }
            } else {
                // we already have a url
                $link = $video;
            }

            // if we have a URL, parse it down
            if (!empty($link)) {

                // initial values
                $video_id = NULL;
                $videoIdRegex = NULL;
                $results = array();

                // check for type of youtube link
                if (strpos($link, 'youtu') !== FALSE) {
                    if (strpos($link, 'youtube.com/watch?v=') !== FALSE) {
                      // works on:
                      // http://www.youtube.com/watch?v=VIDEOID
                      parse_str( parse_url( $link, PHP_URL_QUERY ), $parsed_url );
                    } else if (strpos($link, 'youtube.com') !== FALSE) {
                        // works on:
                        // http://www.youtube.com/embed/VIDEOID
                        // http://www.youtube.com/embed/VIDEOID?modestbranding=1&amp;rel=0
                        // http://www.youtube.com/v/VIDEO-ID?fs=1&amp;hl=en_US
                        $videoIdRegex = '/youtube.com\/(?:embed|v){1}\/([a-zA-Z0-9_-]+)\??/i';
                    } else if (strpos($link, 'youtu.be') !== FALSE) {
                        // works on:
                        // http://youtu.be/daro6K6mym8
                        $videoIdRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
                    }

                    if ($videoIdRegex !== NULL) {
                        if (preg_match($videoIdRegex, $link, $results)) {
                            $video_str = 'https://www.youtube.com/watch/%s';
                            $video_id = $results[1];
                        }
                    } else if (!empty($parsed_url['v'])) {
                      $video_str = 'https://www.youtube.com/watch/%s';
                      $video_id = $parsed_url['v'];
                    }
                }

                // handle vimeo videos
                else if (strpos($video, 'vimeo') !== FALSE) {
                    if (strpos($video, 'player.vimeo.com') !== FALSE) {
                        // works on:
                        // http://player.vimeo.com/video/37985580?title=0&amp;byline=0&amp;portrait=0
                        $videoIdRegex = '/player.vimeo.com\/video\/([0-9]+)\??/i';
                    } else {
                        // works on:
                        // http://vimeo.com/37985580
                        $videoIdRegex = '/vimeo.com\/([0-9]+)\??/i';
                    }

                    if ($videoIdRegex !== NULL) {
                        if (preg_match($videoIdRegex, $link, $results)) {
                            $video_id = $results[1];

                            // get the thumbnail
                            try {
                                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_id.php"));
                                if (!empty($hash) && is_array($hash)) {
                                    $video_str = 'https://vimeo.com/%s';
                                } else {
                                    // don't use, couldn't find what we need
                                    unset($video_id);
                                }
                            } catch (Exception $e) {
                                unset($video_id);
                            }
                        }
                    }
                }

                // check if we have a video id, if so, add the video metadata
                if (!empty($video_id)) {
                    // add to return
                    $videos[] = sprintf($video_str, $video_id);
                }
            }

        }

    }

    $originals = array();
    // remove duplicates
    foreach ($videos as $video_key => $video_url) {
      if (!in_array($video_url, $originals)) {
        $originals[$video_key] = $video_url;
      } else {
        // If already in array
        unset($videos[$video_key]);
      }
    }

    // return array of parsed videos
    return $videos;
}

/**
 * DISPLAY META FIELD
 * --
 * Displays meta field value along with label
 */
function ahsnnf_display_meta_field($label, $data) {
  echo '<div class="ahsnnf-meta-field"><strong>' . $label . '</strong> ' . $data . '</div>';
}


/**
 * FIND INTRODUCTION IN CONTENT
 * --
 * Parses string to find any "overview" heading and then splits the content before and after this
 */
function ahsnnf_find_intro($content) {
  // Firstly parse content with headings regex
  preg_match_all("#<h(\d)[^>]*?>(.*?)<[^>]*?/h\d>#i",$content,$headings, PREG_SET_ORDER);
  $body_split = '';
  // If any headings are found
  if (!empty($headings)) {
    $intro_heading = false;
    // Loop through headings
    foreach ($headings as $heading) {
      // If heading includes the word "introduction"
      if (strpos(strtolower($heading[0]), 'introduction') !== false) {
        // Then this section is the intro, set var
        $intro_heading = $heading[0];
      // If this heading includes the word "overview" or is the heading after the introduction
      // this heading marks the start of the main section
      } else if ($intro_heading || strpos(strtolower($heading[0]), 'overview') !== false) {
        // Split the body
        $body_split = $heading[0];
        break;
      }
    }
    // Split into intro and overview if split location was found
    if ($body_split != '') {
      $separated_content = explode($body_split, $content);
      if (!empty($separated_content[0]) && !empty($separated_content[1]) && strlen($separated_content[0]) < 1200) {
        // If intro heading was found
        if ($intro_heading) {
          // Remove introduction heading from content
          $separated_content[0] = str_replace($intro_heading, '', $separated_content[0]);
        }
        // Remove tags from introduction too
        $separated_content[0] = strip_tags($separated_content[0]);
        // Return both intro and main content as array
        return $separated_content;
      }
    }
  }
  return false;
}