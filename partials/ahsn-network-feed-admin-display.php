<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    AHSN_Network_Feed
 * @subpackage AHSN_Network_Feed/partials
 */
?>
<div class="wrap">
  <div id="icon-themes" class="icon32"></div>
  <h2>AHSN Network IE Feed</h2>
  <div class="welcome-panel" style="max-width: 860px;">
    <div class="welcome-panel-content">
      <p style="margin-top: -8px;">This plugin allows you to select content from within your WordPress site to share with the <a href="https://www.ahsninnovationexchange.co.uk/" target="blank">National AHSN Innovation Exchange</a>. This means you can create content in your current site and it will automatically be shared with the national site, saving you valuable time.</p>
      <p>We will only share published and public content that you select to the National AHSN Innovation Exchange and this plugin should not affect any existing functionality within your website. If you do have any issues with this plugin, please <a href="mailto:web@cynergy.co.uk">email us</a>.</p>
    </div>
	</div>
  <?php settings_errors(); ?>
  <?php if (ahsn_network_feed_settings_check()) :?>
    <h2>Here's what you're currently sharing to the National AHSN Innovation Exchange:</h2>
    <?php
    $args = array(
      'showposts' => 2,
      'post_status'=> 'publish',
      'ignore_sticky_posts' => true,
    );
    if (get_option('ahsn_network_feed_field_news')) {
      // Get options from settings
      echo '<h3><span class="dashicons dashicons-yes-alt"></span> News</h3>';
      $news_args = $args;
      $news_args['post_type'] = get_option('ahsn_network_feed_field_news');
      $news_categories = get_option('ahsn_network_feed_field_news_categories');
      ahsn_network_feed_show_dash_posts($news_args, $news_categories, 'news');
    }
    if (get_option('ahsn_network_feed_field_events')) {
      // Get options from settings
      echo '<h3><span class="dashicons dashicons-yes-alt"></span> Events</h3>';
      $events_args = $args;
      $events_args['post_type'] = get_option('ahsn_network_feed_field_events');
      $events_categories = get_option('ahsn_network_feed_field_events_categories');
      ahsn_network_feed_show_dash_posts($events_args, $events_categories, 'events');
    }
    if (get_option('ahsn_network_feed_field_innovations')) {
      // Get options from settings
      echo '<h3><span class="dashicons dashicons-yes-alt"></span> Innovations</h3>';
      $innovations_args = $args;
      $innovations_args['post_type'] = get_option('ahsn_network_feed_field_innovations');
      $innovations_categories = get_option('ahsn_network_feed_field_innovations_categories');
      ahsn_network_feed_show_dash_posts($innovations_args, $innovations_categories, 'innovations');
    }
    if (get_option('ahsn_network_feed_field_challenges')) {
      // Get options from settings
      echo '<h3><span class="dashicons dashicons-yes-alt"></span> Challenges</h3>';
      $challenges_args = $args;
      $challenges_args['post_type'] = get_option('ahsn_network_feed_field_challenges');
      $challenges_categories = get_option('ahsn_network_feed_field_challenges_categories');
      ahsn_network_feed_show_dash_posts($challenges_args, $challenges_categories, 'challenges');
    }
    ?>
  <?php else: ?>
    <p>No content has been selected to share yet.</p>
  <?php endif; ?>
  <a href="/wp-admin/admin.php?page=ahsn-network-feed-settings" class="button button-primary">Edit settings</a>
</div>
<style>
  .postbox-container .card > img.wp-post-image:first-child {
    margin-top:10px;
  }
  .ahsnnf-meta-field {
    margin-bottom:10px
  }
  .ahsnnf-meta-field img {
    max-width:100%;
    height:auto;
  }
</style>