<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    AHSN_Network_Feed
 * @subpackage AHSN_Network_Feed/partials
 */
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
  <div id="icon-themes" class="icon32"></div>
  <h2>AHSN Network IE Feed Settings</h2>
  <!--NEED THE settings_errors below so that the errors/success messages are shown after submission - wasn't working once we started using add_menu_page and stopped using add_options_page so needed this-->
  <?php settings_errors(); ?>
  <form method="POST" action="options.php" class="ahsn-network-feed-settings">
    <?php
    settings_fields( 'ahsn_network_feed_general_settings' );
    //do_settings_sections( 'ahsn_network_feed_general_settings' );
    global $wp_settings_sections, $wp_settings_fields;
    $page = 'ahsn_network_feed_general_settings';
    if ( ! isset( $wp_settings_sections[ $page ] ) ) {
        return;
    }

    foreach ( (array) $wp_settings_sections[ $page ] as $section ) {
        echo '<div class="card">';

        if ( $section['title'] ) {
            echo "<h2>{$section['title']}</h2>\n";
        }

        if ( $section['callback'] ) {
            call_user_func( $section['callback'], $section );
        }

        if ( ! isset( $wp_settings_fields ) || ! isset( $wp_settings_fields[ $page ] ) || ! isset( $wp_settings_fields[ $page ][ $section['id'] ] ) ) {
            continue;
        }
        echo '<table class="form-table" role="presentation">';
        do_settings_fields( $page, $section['id'] );
        echo '</table>';

        $option_var = 'ahsn_network_feed_field_'.strtolower($section['title']);
        $title = ($section['title'] == 'News') ? 'News posts' : $section['title'];
        if (get_option($option_var)) {
          echo '<div class="sharing-notice">
            <p><span class="dashicons dashicons-share-alt"></span> <strong>SHARING:</strong> '.$title.' are now being shared to the national site. You can also <a href="/wp-admin/admin.php?page=ahsn-network-feed">check what data is being shared via the feed.</a></p>
          </div>';
        } else {
          echo '<div class="sharing-notice sharing-notice--warning">
            <p><strong>NOT SET:</strong> No '.$title.' are currently being shared.</a></p>
          </div>';
        }

        echo '</div>';
    }
    submit_button();
    ?>
  </form>
  <style>
    .ahsn-network-feed-settings .card {
      max-width:100%;
    }
    .ahsn-network-feed-settings .card .form-table tr:last-child {
      border-bottom:0;
    }
    .ahsn-network-feed-settings .card .form-table tr:first-child th,
    .ahsn-network-feed-settings .card .form-table tr:first-child td {
      background:none;
    }
    .ahsn-network-feed-settings .card .form-table tr:not(:first-child) th {
      padding-left:15px;
      border-left: 1px solid rgba(0, 0, 0, 0.15);
    }
    .ahsn-network-feed-settings .card .form-table tr:not(:first-child) td {
      padding-right:15px;
      border-right: 1px solid rgba(0, 0, 0, 0.15);
    }
    .ahsn-network-feed-settings .card .form-table tr th,
    .ahsn-network-feed-settings .card .form-table tr td {
      background:#f1f1f1;
    }
    .ahsn-network-feed-settings .form-table tr {
      border-bottom: 1px solid rgba(0, 0, 0, 0.15);
    }
    .sharing-notice {
      background: #eeffea;
      color: #174c09;
      border: 1px solid #ccd0d4;
      box-shadow: 0 1px 1px rgba(0,0,0,.04);
      margin: 0 0 2px;
      padding: 1px 12px;
    }
    .sharing-notice.sharing-notice--warning {
      background: #fbf6e4;
      color: #4c2809;
    }
  </style>
</div>