== AHSN Network Feed WordPress plugin ==
Requires at least: 5.0
Tested up to: 5.7.1
Requires PHP: 7.2
Stable tag: 1.1.7

This plugin allows you to select content from within your WordPress site to share with the ahsninnovationexchange.co.uk

== Description ==
This plugin allows you to select content from within your WordPress site to share with the [National AHSN Innovation Exchange](https://www.ahsninnovationexchange.co.uk). This means you can create content in your current site and it will automatically be shared with the national site, saving you valuable time.

We will only share published and public content that you select to the National AHSN Innovation Exchange and this plugin should not affect any existing functionality within your website.

== Changelog ==

= 1.1.7 =

* Fix to handling of no content

= 1.1.6 =

* Added workaround for any posts with multi-field content

= 1.1.5 =

* Tested with WP 5.7.1

= 1.1.4 =

* Various minor bugfixes

= 1.1.3 =

* Bugfixes

= 1.1.2 =

* Added mail function which emails developer when its set up

= 1.1.1 =

* Fix to handling of event date values

= 1.1.0 =

* Minor improvements

= 1.0.8 =

* Further improved handling of varying body content

= 1.0.7 =

* Improved handling of varying body content

= 1.0.6 =

* Added WordPress readme

= 1.0.5 =

* Multiple optimisations and testing

= 1.0.4 =

* Improve display of what is being shared

= 1.0.3 =

* All required features now implemented

= 1.0.2 =

* Major functionality updates

= 1.0.1 =

* Minor updates

= 1.0.0 =

* Initial plugin release