<?php defined( 'ABSPATH' ) or exit;
/**
 * AHSN Network IE Feed plugin
 *
 * Plugin Name:        AHSN Network IE Feed
 * Description:        Link up your website to ahsninnovationexchange.co.uk to allow innovations, challenges, events and news to be automatically shared to the innovation exchange network.
 * Version:            1.1.7
 * Author:             Cynergy
 * Author URI:         https://www.cynergy.co.uk
 * Text Domain:        cynergy-ahsn-network-feed
 * Requires at least:  5.0
 * Tested up to:       5.7.1
 *
 * @package   AHSN_Network_Feed
 * @version   1.1.7
 * @author    Cynergy <web@cynergy.co.uk>
 * @link      https://www.cynergy.co.uk
 *
 * Singleton class for setting up the plugin.
 *
 * @since  1.0.0
 * @access public
 */
final class AHSN_Network_Feed {

	/**
	 * Minimum required PHP version.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
  private $php_version = '7.2.0';

	/**
	 * Plugin directory path.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $dir = '';

	/**
	 * Plugin directory URI.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $uri = '';

	/**
	 * Plugin name
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $plugin_name = 'AHSN Network IE Feed';

	/**
	 * Plugin slug
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $plugin_slug = 'ahsn-network-feed';

	/**
	 * Constructor method.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
  private function __construct() {
		add_action('admin_init', array( $this, 'registerAndBuildFields' ));
	}

	/**
	 * Sets up globals.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup() {

		// Main plugin directory path and URI.
		$this->dir = trailingslashit( plugin_dir_path( __FILE__ ) );
		$this->uri  = trailingslashit( plugin_dir_url(  __FILE__ ) );
	}

  /**
	 * Loads files needed by the plugin.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function includes() {

		// Check if we meet the minimum PHP version.
		if ( version_compare( PHP_VERSION, $this->php_version, '<' ) ) {

			// Add admin notice.
			add_action( 'admin_notices', array( $this, 'php_admin_notice' ) );

			// Bail.
			return;
		}

		require 'plugin-update-checker/plugin-update-checker.php';
		$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
			'https://bitbucket.org/PixelPie/ahsn-network-feed',
			__FILE__,
			$this->plugin_slug
		);

		//Optional: Set the branch that contains the stable release.
		$myUpdateChecker->setBranch('stable-release');

		// Load includes files.
		require_once( $this->dir . 'inc/functions.php');
		require_once( $this->dir . 'inc/rss.php');

		// Load menu
		add_action('admin_menu', array( $this, 'addPluginAdminMenu' ), 9);

		// Add check for plugin setup
		add_action('wp_loaded', array( $this, 'checkPluginFeedLive' ));

	}

	public function addPluginAdminMenu() {
		//add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
		add_menu_page(  $this->plugin_name, $this->plugin_name, 'administrator', $this->plugin_slug, array( $this, 'displayPluginAdminDashboard' ), 'dashicons-rss', 26 );

		//add_submenu_page( '$parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
		add_submenu_page( $this->plugin_slug, $this->plugin_name.' Settings', 'Settings', 'administrator', $this->plugin_slug.'-settings', array( $this, 'displayPluginAdminSettings' ));

		if (!ahsn_network_feed_settings_check()) {
			$screen = get_current_screen();
			if(is_admin()) {
				// If settings aren't filled in, show notice.
				printf(
					'<div class="notice notice-warning is-dismissible"><p><strong>%s</strong> <a href="/wp-admin/admin.php?page=ahsn-network-feed-settings">Go to settings</a>.</p></div>',
					esc_html( 'Please configure the '. $this->plugin_name.' by selecting the content you wish to share.' )
				);
			}
		}
	}

	public function checkPluginFeedLive() {
		// Check if we've been notified about the plugin having been setup
		if (empty(get_option('ahsnnf_plugin_is_live'))) {
			// Check that this has options filled in
			if (ahsn_network_feed_settings_check()) {
				$msg = 'Hello,<br><br>' . get_bloginfo('name') . ' has now set up the AHSN Network IE Feed plugin, and is accessible at this url: ' . strip_tags(get_bloginfo('url')) . '<br><br>Thank you,<br>AHSN Network IE Feed plugin team';
				$headers = array('Content-Type: text/html; charset=UTF-8');
				wp_mail('web@cynergy.co.uk', 'AHSN Network IE Feed plugin', $msg, $headers);
				add_option('ahsnnf_plugin_is_live', time());
			}
		}
	}

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {

		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self;
			$instance->setup();
			$instance->includes();
		}

		return $instance;
	}

	/**
	 * Returns a message noting the minimum version of PHP required.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function get_min_php_message() {

		return sprintf(
			__( $plugin_name . ' requires PHP version %1$s. You are running version %2$s. Please upgrade and try again.', 'cynergy-ahsn-network-feed' ),
			$this->php_version,
			PHP_VERSION
		);
  }

	/**
	 * Outputs the admin notice that the user needs to upgrade their PHP version. It also
	 * auto-deactivates the plugin.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function php_admin_notice() {

		// Output notice.
		printf(
			'<div class="notice notice-error is-dismissible"><p><strong>%s</strong></p></div>',
			esc_html( $this->get_min_php_message() )
		);

		// Make sure the plugin is deactivated.
		deactivate_plugins( plugin_basename( __FILE__ ) );
	}

	/**
	 * Outputs the admin notice that the user needs to upgrade their PHP version. It also
	 * auto-deactivates the plugin.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function displayPluginAdminDashboard() {
		require_once 'partials/'.$this->plugin_slug.'-admin-display.php';
	}

	public function displayPluginAdminSettings() {
		// set this var to be used in the settings-display view
		$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general';
		if(isset($_GET['error_message'])){
				add_action('admin_notices', array($this,'pluginNameSettingsMessages'));
				do_action( 'admin_notices', $_GET['error_message'] );
		}
		require_once 'partials/'.$this->plugin_slug.'-settings-display.php';
	}

	public function ahsnNetworkFeedSettingsMessages($error_message){
		switch ($error_message) {
				case '1':
						$message = __( 'There was an error adding this setting. Please try again. If this persists, email us at <a href="mailto:web@cynergy.co.uk">web@cynergy.co.uk</a>.', 'my-text-domain' );
						$err_code = esc_attr( 'ahsn_network_feed_example_setting' );
						$setting_field = 'ahsn_network_feed_example_setting';
						break;
		}
		$type = 'error';
		add_settings_error(
					 $setting_field,
					 $err_code,
					 $message,
					 $type
			 );
	}

	public function retrieveAllTypes() {

		// Get a list of all content types within wordpress
		$types = get_post_types(array(
			'public' => true,
		), 'objects');
		unset($types['acf-field']);
		unset($types['wp_block']);
		unset($types['user_request']);
		unset($types['oembed_cache']);
		unset($types['customize_changeset']);
		unset($types['custom_css']);
		unset($types['nav_menu_item']);
		unset($types['revision']);
		unset($types['attachment']);

		$readable_types = [];

		foreach($types as $typekey => $type) {
			$readable_types[$typekey] = $type->label;
		}

		return $readable_types;

	}

	public function retrieveCategories($type) {
		// Get a list of all categories within each type
		$taxonomies = get_object_taxonomies($type);

		$readable_taxonomies = [];

		foreach ($taxonomies as $tax) {
			$tax_terms = get_terms( $tax , array('hide_empty' => false));
			foreach($tax_terms as $term) {
				$readable_taxonomies[$tax][$term->term_id] = $term->name;
			}
		}

		return $readable_taxonomies;

	}

	public function registerAndBuildFields() {
		if (is_admin()) {
			$types = $this->retrieveAllTypes();
			$pluginref = str_replace('-', '_', $this->plugin_slug);

			/**
			 * First, we add_settings_section. This is necessary since all future settings must belong to one.
			 * Second, add_settings_field
			 * Third, register_setting
			 */
			add_settings_section(
				// ID used to identify this section and with which to register options
				'ahsn_network_feed_content',
				// Title to be displayed on the administration page
				'News',
				// Callback used to render the description of the section
				array( $this, 'ahsn_network_feed_display_general_account' ),
				// Page on which to add this section of options
				'ahsn_network_feed_general_settings'
			);
			add_settings_section(
				// ID used to identify this section and with which to register options
				'ahsn_network_feed_events',
				// Title to be displayed on the administration page
				'Events',
				// Callback used to render the description of the section
				array( $this, 'ahsn_network_feed_display_event_account' ),
				// Page on which to add this section of options
				'ahsn_network_feed_general_settings'
			);
			add_settings_section(
				// ID used to identify this section and with which to register options
				'ahsn_network_feed_innovations',
				// Title to be displayed on the administration page
				'Innovations',
				// Callback used to render the description of the section
				array( $this, 'ahsn_network_feed_display_innovation_account' ),
				// Page on which to add this section of options
				'ahsn_network_feed_general_settings'
			);
			add_settings_section(
				// ID used to identify this section and with which to register options
				'ahsn_network_feed_challenges',
				// Title to be displayed on the administration page
				'Challenges',
				// Callback used to render the description of the section
				array( $this, 'ahsn_network_feed_display_challenge_account' ),
				// Page on which to add this section of options
				'ahsn_network_feed_general_settings'
			);

			/**
			 * NEWS
			 */

			unset($args);
			$args = array (
				'type'      => 'select',
				'id'    => 'ahsn_network_feed_field_news',
				'name'      => 'ahsn_network_feed_field_news',
				'get_option_list' => $types,
				'default' => 'post',
				'value_type' => 'normal',
				'required' => 'false',
				'wp_data' => 'option',
				'data_type_name' => 'content types',
				'description' => __( 'Select the content type that contains your news posts', 'ahsn-network-plugin' ),
			);
			add_settings_field(
				'ahsn_network_feed_field_news',
				'Select News posts',
				array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_content',
				$args // ARGUMENTS
			);

			register_setting(
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_field_news'
			);

			$news_option = get_option($pluginref . '_field_news');
			if (!empty(get_option($pluginref . '_field_news'))) {

				$categories = $this->retrieveCategories($news_option);
				if (!empty($categories)) {

					unset($args);
					$args = array (
						'type'      => 'checkboxes',
						'id'    => $pluginref . '_field_news_categories',
						'name'      => $pluginref . '_field_news_categories',
						'get_option_list' => $categories,
						'default' => array(),
						'value_type' => 'serialized',
						'required' => 'false',
						'wp_data' => 'option',
						'data_type_name' => 'categories',
						'description'  => __( 'If applicable, select the specific categories you wish to share. If none are selected, all will be shared regardless of category.', 'ahsn-network-plugin' ),
					);
					add_settings_field(
						'ahsn_network_feed_field_news_categories',
						'Filter by categories',
						array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_content',
						$args // ARGUMENTS
					);

					register_setting(
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_field_news_categories'
					);
				}

			}


			/**
			 * EVENTS
			 */

			unset($args);
			$args = array (
				'type'      => 'select',
				'id'    => 'ahsn_network_feed_field_events',
				'name'      => 'ahsn_network_feed_field_events',
				'get_option_list' => $types,
				'default' => '',
				'value_type' => 'normal',
				'required' => 'false',
				'wp_data' => 'option',
				'data_type_name' => 'content types',
				'description' => __( 'Select the content type that contains your events', 'ahsn-network-plugin' ),
			);
			add_settings_field(
				'ahsn_network_feed_field_events',
				'Select Events',
				array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_events',
				$args // ARGUMENTS
			);
			register_setting(
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_field_events'
			);

			$events_option = get_option($pluginref . '_field_events');
			if (!empty($events_option)) {
				$event_categories = $this->retrieveCategories($events_option);
				if (!empty($event_categories)) {
					unset($args);
					$args = array (
						'type'      => 'checkboxes',
						'id'    => 'ahsn_network_feed_field_events_categories',
						'name'      => 'ahsn_network_feed_field_events_categories',
						'get_option_list' => $event_categories,
						'default' => array(),
						'value_type' => 'serialized',
						'required' => 'false',
						'wp_data' => 'option',
						'description'  => __( 'If applicable, select the specific categories you wish to share. If none are selected, all will be shared regardless of category.', 'ahsn-network-plugin' ),
					);
					add_settings_field(
						'ahsn_network_feed_field_events_categories',
						'Filter by categories',
						array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_events',
						$args // ARGUMENTS
					);

					register_setting(
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_field_events_categories'
					);
				}

				/**
				 * EVENTS FIELDS -- Date
				 */
				$events_meta_keys = ahsn_network_feed_get_meta_keys($events_option);
				$events_field_options = [];
				foreach($events_meta_keys as $key => $meta) {
					$events_field_options[$meta] = $meta;
				}


				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_events_date',
					'name'      => $pluginref . '_field_events_date',
					'get_option_list' => $events_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'Please select the field which stores the event date for your event', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_events_date',
					'Event date field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_events',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_events_date'
				);


				/**
				 * EVENTS FIELDS -- Register link
				 */
				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_events_register',
					'name'      => $pluginref . '_field_events_register',
					'get_option_list' => $events_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'Please select the field which stores the registration link for your event', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_events_register',
					'Register link field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_events',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_events_register'
				);
			}


			/**
			 * CHALLENGES
			 */

			unset($args);
			$args = array (
				'type'      => 'select',
				'id'    => 'ahsn_network_feed_field_challenges',
				'name'      => 'ahsn_network_feed_field_challenges',
				'get_option_list' => $types,
				'default' => 'post',
				'value_type' => 'normal',
				'required' => 'false',
				'wp_data' => 'option',
				'data_type_name' => 'content types',
				'description' => __( 'Select the content type that contains your challenges', 'ahsn-network-plugin' ),
			);
			add_settings_field(
				'ahsn_network_feed_field_challenges',
				'Select challenges',
				array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_challenges',
				$args // ARGUMENTS
			);

			register_setting(
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_field_challenges'
			);

			$challenges_option = get_option($pluginref . '_field_challenges');
			if (!empty(get_option($pluginref . '_field_challenges'))) {

				$challenges_categories = $this->retrieveCategories($challenges_option);
				if (!empty($challenges_categories)) {

					unset($args);
					$args = array (
						'type'      => 'checkboxes',
						'id'    => $pluginref . '_field_challenges_categories',
						'name'      => $pluginref . '_field_challenges_categories',
						'get_option_list' => $challenges_categories,
						'default' => array(),
						'value_type' => 'serialized',
						'required' => 'false',
						'wp_data' => 'option',
						'description'  => __( 'If applicable, select the specific categories you wish to share. If none are selected, all will be shared regardless of category.', 'ahsn-network-plugin' ),
					);
					add_settings_field(
						'ahsn_network_feed_field_challenges_categories',
						'Filter by categories',
						array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_challenges',
						$args // ARGUMENTS
					);

					register_setting(
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_field_challenges_categories'
					);
				}

			}

			/**
			 * INNOVATIONS
			 */

			unset($args);
			$args = array (
				'type'      => 'select',
				'id'    => 'ahsn_network_feed_field_innovations',
				'name'      => 'ahsn_network_feed_field_innovations',
				'get_option_list' => $types,
				'default' => '',
				'value_type' => 'normal',
				'required' => 'false',
				'wp_data' => 'option',
				'data_type_name' => 'content types',
				'description' => __( 'Select the content type that contains your innovations', 'ahsn-network-plugin' ),
			);
			add_settings_field(
				'ahsn_network_feed_field_innovations',
				'Select Innovations',
				array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_innovations',
				$args // ARGUMENTS
			);
			register_setting(
				'ahsn_network_feed_general_settings',
				'ahsn_network_feed_field_innovations'
			);

			$innovations_option = get_option($pluginref . '_field_innovations');
			if (!empty($innovations_option)) {
				$innovations_categories = $this->retrieveCategories($innovations_option);
				if (!empty($innovations_categories)) {
					unset($args);
					$args = array (
						'type'      => 'checkboxes',
						'id'    => 'ahsn_network_feed_field_innovations_categories',
						'name'      => 'ahsn_network_feed_field_innovations_categories',
						'get_option_list' => $innovations_categories,
						'default' => array(),
						'value_type' => 'serialized',
						'required' => 'false',
						'wp_data' => 'option',
						'description'  => __( 'If applicable, select the specific categories you wish to share. If none are selected, all will be shared regardless of category.', 'ahsn-network-plugin' ),
					);
					add_settings_field(
						'ahsn_network_feed_field_innovations_categories',
						'Filter by categories',
						array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_innovations',
						$args // ARGUMENTS
					);

					register_setting(
						'ahsn_network_feed_general_settings',
						'ahsn_network_feed_field_innovations_categories'
					);
				}


				/**
				 * INNOVATIONS FIELDS -- Introduction
				 */
				$innovations_meta_keys = ahsn_network_feed_get_meta_keys($innovations_option);
				$innovations_field_options = [];
				foreach($innovations_meta_keys as $key => $meta) {
					$innovations_field_options[$meta] = $meta;
				}
				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_innovations_intro',
					'name'      => $pluginref . '_field_innovations_intro',
					'get_option_list' => $innovations_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'If available, please select a field that holds an introduction or summary to your innovation.', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_innovations_intro',
					'Introduction field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_innovations',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_innovations_intro'
				);


				/**
				 * INNOVATIONS FIELDS -- Main content
				 */
				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_innovations_content',
					'name'      => $pluginref . '_field_innovations_content',
					'get_option_list' => $innovations_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'If the main content for your innovation isn\'t in the main post body, please select a field that holds the main content for your innovation.', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_innovations_content',
					'Overview field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_innovations',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_innovations_content'
				);

				/**
				 * INNOVATIONS FIELDS -- Organisation
				 */
				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_innovations_organisation',
					'name'      => $pluginref . '_field_innovations_organisation',
					'get_option_list' => $innovations_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'If available, please select a field that holds the name of the organisation responsible for the innovation.', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_innovations_organisation',
					'Organisation field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_innovations',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_innovations_organisation'
				);


				/**
				 * INNOVATIONS FIELDS -- Images
				 */
				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_innovations_images',
					'name'      => $pluginref . '_field_innovations_images',
					'get_option_list' => $innovations_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'If available, please select a field that holds any images associated with the innovation.', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_innovations_images',
					'Images field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_innovations',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_innovations_images'
				);


				/**
				 * INNOVATIONS FIELDS -- Videos
				 */
				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_innovations_videos',
					'name'      => $pluginref . '_field_innovations_videos',
					'get_option_list' => $innovations_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'If available, please select a field that holds any videos associated with the innovation.<br><small>Please note, only YouTube and Vimeo videos are supported at this time.</small>', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_innovations_videos',
					'Videos field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_innovations',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_innovations_videos'
				);


				/**
				 * INNOVATIONS FIELDS -- Files
				 */
				unset($args);
				$args = array (
					'type'      => 'select',
					'id'    => $pluginref . '_field_innovations_files',
					'name'      => $pluginref . '_field_innovations_files',
					'get_option_list' => $innovations_field_options,
					'default' => array(),
					'value_type' => 'normal',
					'required' => 'false',
					'wp_data' => 'option',
					'data_type_name' => 'fields',
					'description'  => __( 'If available, please select a field that holds any files associated with the innovation.', 'ahsn-network-plugin' ),
				);
				add_settings_field(
					'ahsn_network_feed_field_innovations_files',
					'Files field',
					array( $this, 'ahsn_network_feed_render_settings_field' ), // CALLBACK
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_innovations',
					$args // ARGUMENTS
				);

				register_setting(
					'ahsn_network_feed_general_settings',
					'ahsn_network_feed_field_innovations_files'
				);

			}
		} else {
			global $wp_query;
    	$wp_query->set_404();
		}

	}

	public function ahsn_network_feed_display_general_account() {
		echo '<p>These are the news articles within your site that can be synced to the <a href="https://www.ahsninnovationexchange.co.uk/news/ahsn" target="_blank">National AHSN IE news page</a></p>';
	}

	public function ahsn_network_feed_display_event_account() {
	  echo '<p>These are any events within your site that can be synced to the <a href="https://www.ahsninnovationexchange.co.uk/news/events" target="_blank">National AHSN IE events page</a></p>';
	}

	public function ahsn_network_feed_display_innovation_account() {
		echo '<p>These are the innovations within your site that can be synced to the <a href="https://www.ahsninnovationexchange.co.uk/innovations" target="_blank">National AHSN IE innovations page</a></p>';
	}

	public function ahsn_network_feed_display_challenge_account() {
		echo '<p>These are the innovations within your site that can be synced to the <a href="https://www.ahsninnovationexchange.co.uk/challenges" target="_blank">National AHSN IE challenges page</a></p>';
	}

	public function ahsn_network_feed_render_settings_field($args) {

		$options = $args['get_option_list'];
		if($args['wp_data'] == 'option'){
			$wp_data_value = get_option($args['name']);
		} elseif($args['wp_data'] == 'post_meta'){
			$wp_data_value = get_post_meta($args['post_id'], $args['name'], true );
		}

		if(!empty($args['description']) && (empty($wp_data_value) || $wp_data_value == '')) {
			if ($args['type'] == 'select' && empty($options)) {

			} else {
				echo '<p style="margin-bottom:8px;">'.$args['description'].'</p>';
			}
		}

		switch ($args['type']) {

			case 'input':
				$value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;
				if($args['subtype'] != 'checkbox'){
					$prependStart = (isset($args['prepend_value'])) ? '<div class="input-prepend"> <span class="add-on">'.$args['prepend_value'].'</span>' : '';
					$prependEnd = (isset($args['prepend_value'])) ? '</div>' : '';
					$step = (isset($args['step'])) ? 'step="'.$args['step'].'"' : '';
					$min = (isset($args['min'])) ? 'min="'.$args['min'].'"' : '';
					$max = (isset($args['max'])) ? 'max="'.$args['max'].'"' : '';
					if(isset($args['disabled'])){
						// hide the actual input bc if it was just a disabled input the informaiton saved in the database would be wrong - bc it would pass empty values and wipe the actual information
						echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'_disabled" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'_disabled" size="40" disabled value="' . esc_attr($value) . '" /><input type="hidden" id="'.$args['id'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
					} else {
						echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
					}
					/*<input required="required" '.$disabled.' type="number" step="any" id="'.$this->plugin_slug.'_cost2" name="'.$this->plugin_slug.'_cost2" value="' . esc_attr( $cost ) . '" size="25" /><input type="hidden" id="'.$this->plugin_slug.'_cost" step="any" name="'.$this->plugin_slug.'_cost" value="' . esc_attr( $cost ) . '" />*/
				} else {
					$checked = ($value) ? 'checked' : '';
					echo '<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" name="'.$args['name'].'" size="40" value="1" '.$checked.' />';
				}

			break;

			case 'select':

				$value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;

				if (empty($options)) {
					$fields_extra = ($args['data_type_name'] == 'fields') ? ' These content items may need to be edited on the National AHSN site when imported, as without fields we cannot separate the specific parts of the content.' : '' ;
					echo '<small>No '.$args['data_type_name'].' available.'.$fields_extra.'</small>';
				} else {

					echo "<select id='".$args['id']."' name='".$args['name']."'>";
					echo "<option value=''>- None -</option>";
					foreach($options as $optionkey => $type) {
						$selected = ($value==$optionkey) ? 'selected="selected"' : '';
						echo "<option value='$optionkey' $selected>".ahsn_network_feed_readable_metakey($type)."</option>";
					}
					echo "</select>";

				}

			break;

			case 'checkboxes':

				$value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;

				foreach($args['get_option_list'] as $taxonomy => $terms) {
					$taxonomy_details = get_taxonomy( $taxonomy );
					if (!empty($taxonomy_details->label)) {
						echo '<h4 style="margin-top:5px;">'.$taxonomy_details->label.'</h4>';
					}

					foreach($terms as $catkey => $cat) {
						$sel = false;
						$checkbox_value = $taxonomy . "--" . $catkey;
						if (!empty($wp_data_value) && is_array($wp_data_value)) {
							foreach ($wp_data_value as $sel_key => $sel_value) {
								if ($checkbox_value == $sel_value) {
									$sel = true;
								}
							}
						}
						$selected = ($sel) ? 'checked="checked"' : '';
						echo "<label style='display:inline-block;margin-right:15px;margin-bottom:20px;'><input type='checkbox' name='".$args['name']."[]' value='" . $taxonomy . "--" . $catkey . "' " . $selected . ">$cat </label>";
					}
				}

			break;

			default:
				# code...
			break;
		}
	}

}

/**
 * Gets the instance of the `AHSN_Network_Feed` class.  This function is useful for quickly grabbing data
 * used throughout the plugin.
 *
 * @since  1.0.0
 * @access public
 * @return object
 */
function AHSN_Network_Feed_init() {
	return AHSN_Network_Feed::get_instance();
}

// Let's roll!
AHSN_Network_Feed_init();